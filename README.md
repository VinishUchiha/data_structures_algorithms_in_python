# Data_Structures_Algorithms_in_Python

Implementation of Data Structures & Algorithms in Python

1.Linear Search
2.Binary Search
3.Stacks
4.Queues
5.Deques
6.Linked List
7.Circular Linked List
8.Double Linked List
9.Stacks Using Linked List
10.Queues Using Linked List
11.Deques Using Linked List